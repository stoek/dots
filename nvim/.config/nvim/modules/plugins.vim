call plug#begin()
"Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" colorscheme + appearence
Plug 'morhetz/gruvbox'
Plug 'arcticicestudio/nord-vim'

" Lang stuff
Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
Plug 'sbdchd/neoformat'


Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'mhinz/vim-startify'

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'easymotion/vim-easymotion'

Plug 'ervandew/supertab' 

call plug#end()
